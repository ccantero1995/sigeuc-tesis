from SIGEUC.api.models import egresadoModel
from table import Table
from table.columns import Column

class PersonTable(Table):
    id = Column(field='id')
    class Meta:
        model = egresadoModel