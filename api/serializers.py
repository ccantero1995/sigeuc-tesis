from rest_framework import serializers
from api.models import *

class egresadoSerializer(serializers.ModelSerializer):
	class 	Meta:
		model   = egresadoModel
		fields  = [
			'nombreUsuario', 'primerNombre', 'segundoNombre',
			'primerApellido', 'segundoApellido', 'fechaNacimiento',
			'email', 'intereses', 'descripcion', 'foto', 'telefono',
			'identificacion', 'url', 'direccion']

class trabajosSerializer(serializers.ModelSerializer):
	class 	Meta:
		model = trabajosModel
		fields = ['titulo', 'nombre', 'descripcion']
class estudiosUCSerializer(serializers.ModelSerializer):
	class 	Meta:
		model = estudiosUCModel
		fields = ['titulo', 'nombre', 'descripcion']