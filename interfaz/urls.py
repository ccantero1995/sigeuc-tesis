from django.urls import path
from interfaz import views

urlpatterns = [
    path('inicio/', views.inicio),
    path('inicio/egresado/privado/linkedin/', views.linkedin),
    path('inicio/egresado/privado/', views.egresado),
    path('inicio/egresado/cerrar/', views.cerrarCuenta),
    path('inicio/cargador/', views.cargador),
    path('inicio/salir/', views.cerrarSesion),
    path('inicio/digae/salir/', views.cerrarSesion),
    path('inicio/egresado/privado/salir/', views.cerrarSesion),
    path('inicio/vicerrectorado/', views.vicerrectorado),
    path('inicio/vicerrectorado/registrar/', views.registrar),
    path('inicio/vicerrectorado/eliminar/', views.registrar),
    path('inicio/vicerrectorado/salir/', views.alInicio),

    path('formulario/<int:id>', views.formulario),
    path('formulario/salir/', views.alInicio),

    path('inicio/admin/', views.admin),
    path('inicio/admin/salir/', views.alInicio)
]