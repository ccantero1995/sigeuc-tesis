from django.db import models
from django.contrib.auth.models import User


class egresadoModel(models.Model):
	idEgresado              = models.AutoField(primary_key=True)
	user                    = models.OneToOneField( User, on_delete=models.CASCADE)
	intereses               = models.CharField(default = ' ', max_length=300)
	identificacion          = models.CharField(unique = True, max_length=100)
	descripcion             = models.TextField(default = ' ',max_length=500)
	telefono                = models.CharField(max_length=20, default = ' ')
	correo                  = models.EmailField( max_length=30)
	url                     = models.URLField(default = 'www.sigeuc.edu.ve',max_length=300)
	direccion               = models.CharField(max_length=150, default = ' ')
	usada                   = models.BooleanField(default = False)
	linkedin                = models.BooleanField(default = False)
	linkedinToken           = models.CharField(max_length=200, default = ' ')
	localizacionNombre      = models.CharField(max_length=200, default = ' ')
	localizacionDescripcion = models.CharField(max_length=200, default = ' ')
	industria               = models.CharField(max_length=200, default = ' ')
	conexiones              = models.FloatField(default = 0)
	confianza               = models.FloatField(default = 0)


class empresasModel(models.Model):
	idEm      = models.CharField(max_length=100, default = ' ')
	name      = models.CharField(max_length=300, default = ' ')	
	tipo      = models.CharField(max_length=100, default = ' ')
	industria = models.CharField(max_length=40, default = ' ')

class trabajosModel(models.Model):
	id            = models.AutoField(primary_key=True)
	egresadoModel = models.ForeignKey(egresadoModel, on_delete=models.CASCADE)
	titulo        = models.CharField(max_length=100, default = ' ')
	#Empresa
	nombre        = models.CharField(max_length=100, default = ' ')
	descripcion   = models.CharField(max_length=500, default = ' ')
	actualmente   = models.BooleanField(default = False)
	empresa       = models.CharField(max_length=100, default = 'No registrada')

class estudiosUCModel(models.Model):
	id             = models.AutoField(primary_key=True)
	egresadoModel  = models.ForeignKey(egresadoModel, on_delete=models.CASCADE)
	titulo         = models.CharField(max_length=100, default = ' ')
	nombre         = models.CharField(max_length=100, default = ' ')
	fecha          = models.DateField(blank = True, null = True)
	descripcion    = models.CharField(max_length=500, default = ' ')

class consultasModel(models.Model):
	id = models.AutoField(primary_key=True)
	tipo          = models.CharField(max_length=100, default = ' ')
	fecha         = models.DateField(null = False)
	status        = models.CharField(max_length=100, default = ' ')
	count         = models.IntegerField()

class correosModel(models.Model):
	id            = models.AutoField(primary_key=True)
	usuario       = models.CharField(max_length=200, default = ' ')
	status        = models.CharField(max_length=200, default = 'Enviado')
	error         = models.TextField()

class apiModel(models.Model):
	id            = models.AutoField(primary_key=True)
	IDCliente     = models.CharField(max_length=200)
	claveCliente  = models.CharField(max_length=200)

class correoSistemaModel(models.Model):
	id                = models.AutoField(primary_key=True)
	emailTLS          = models.CharField(max_length=200)  
	emailHost         = models.CharField(max_length=200)
	emailPort         = models.CharField(max_length=200)
	emailBackEnd      = models.CharField(max_length=200)
	emailHostUser     = models.CharField(max_length=200)
	emailHostPassword = models.CharField(max_length=200)