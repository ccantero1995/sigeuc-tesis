#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from api.models import *
from api.serializers import *
from api.forms import *
from django.contrib.auth.models import User, Group
from datetime import datetime
from rest_framework.authtoken.models import Token
from django.conf import settings
import  random, string, re, pdb, xlwt, smtplib, numpy as np, scipy.stats as st

dictModSer = {
    'egresado':      {'model':egresadoModel, 'serializer': egresadoSerializer, 'form': egresadoForms},
    'trabajos':      {'model':trabajosModel, 'serializer': trabajosSerializer, 'form': trabajosForms},
    'estudiosUC':    {'model':estudiosUCModel, 'serializer': estudiosUCSerializer, 'form': estudiosUCForms}        
             }


def checkToken(token):

    return Token.objects.filter(key = token).exists()

def item(request, token, id, typeModel):
    
    try:

        user          = Token.objects.get(key = token).user
        instanceEgre  = egresadoModel.objects.get(user = user)

    except:
        return HttpResponse(status=400)    	

    model  = typeModel['model']
    forms  = typeModel['form']
    result = HttpResponse(status=200)

    if request.method == 'POST':

        try:

            instanceModel = model.objects.filter(egresadoModel = instanceEgre).get(id = id)

            form = forms(request.POST, instance = instanceModel)

            if form.is_valid():
                form.save()

            else:
                result = HttpResponse(status=400) 

        except:
            result = HttpResponse(status=400)

    elif  request.method == 'GET':
        
        try:

            instance = model.objects.create(egresadoModel = instanceEgre)
            result     = JsonResponse(data = {'id':instance.id})

        except:
            result   = HttpResponse(status=400)

    elif request.method == "DELETE":
        
        try:
            model.objects.filter(egresadoModel = instanceEgre).get(id = id).delete()
        except:
        	result = HttpResponse(status=400)

    return result

def perfil(request, token):
    
    if not checkToken(token):
        return HttpResponse(status=400)
    user = Token.objects.get(key = token).user
    result   = HttpResponse(status=200)
    
    if request.method == 'POST':

        try:
            
            egresado = egresadoModel.objects.get(user = user)

            user.first_name = request.POST['nombre']
            user.last_name  = request.POST['apellido']

            egresado.telefono       = request.POST['telefono']
            egresado.correo         = request.POST['correo']
            egresado.url            = request.POST['url']
            egresado.direccion      = request.POST['direccion']

            user.save()
            egresado.save()

        except:

            result = HttpResponse(status=400)

    else: 


        if request.method == 'GET':

            egresado = egresadoModel.objects.get(user = user)
            data = {
                "userId": user.id,
                "egresadoId": egresado.idEgresado,
                "nombre": user.first_name,
                "apellido": user.last_name,
                "telefono": egresado.telefono,
                "identificacion": egresado.identificacion,
                "url": egresado.url,
                "direccion": egresado.direccion
                   }
            result = JsonResponse(data = data)
        
        elif request.method == "DELETE":

            user.delete()

        else:

            result = HttpResponse(status=400)

    return result

def trabajo(request, id, token):

    return item(request, token, id, dictModSer['trabajos'] )

def estudioUC(request, id, token):

    return item(request, token, id, dictModSer['estudiosUC'] )
    
def completo(request, itemEgresado):
   
    try:
        
        dictResult = {}
        
        user            = User.objects.get(id = itemEgresado)
        egresado        = egresadoModel.objects.get(user = user)
        trabajos        = trabajosModel.objects.filter(egresadoModel = egresado)
        estudiosUC      = estudiosUCModel.objects.filter(egresadoModel = egresado)

        egresado.usada = True
        egresado.save()
        
        dictResult['perfil'] =  {
            'id'         : itemEgresado,
            'nombre'     : user.first_name,
            'apellido'   : user.last_name,
            'url'        : egresado.url,
            'correo'     : egresado.correo,
            'direccion'  : egresado.direccion,
            'telefono'   : egresado.telefono
                                }

        auxList = []
        for pb in trabajos:
            auxList.append({
                'id'         : pb.id,
                'empresa'    : pb.titulo,
                'cargo'      : pb.nombre,
                'fecha'      : pb.fecha,
                'url'        : pb.url,
                'descripcion': pb.descripcion
            })
        dictResult['trabajos'] = auxList

        auxList = []
        for pb in estudiosUC:
            auxList.append({
                'id'         : pb.id,
                'titulo'     : pb.titulo,
                'facultad'   : pb.nombre,
                'fecha'      : pb.fecha,
                'url'        : pb.url,
                'descripcion': pb.descripcion
            })
        dictResult['estudiosUC'] = auxList

        result = JsonResponse(dictResult, safe = False)

    except:

        result = HttpResponse(status = 400)

    return result

def digaeCarga(request, token):
    
    if checkToken(token):
        email_content = """ Bienvenido, al SIGEUC. Le ha sido asignado un usuario en el mismo, para que, como egresado de la Universidad de Carabobo, pueda ingresar informacion. Su usuario es:  , la clave es:  . Saludos """

        excel_file = request.FILES["egresados"]
        wb = openpyxl.load_workbook(excel_file)
        worksheet = wb["Sheet1"]

        excel_data = [ [ str(columna.value) for columna in fila ] for fila in worksheet.iter_rows()]

        rex   = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
        excel_data = [ row for row in excel_data[1:] if re.match(rex, row[0]) != None]
        errors = []
        grupo = Group.objects.get(name='egresado')
        
        server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        
        import email.message
        msg = email.message.Message()
        msg['Subject'] = 'SIGEUC'
        msg.add_header('Content-Type', 'text/html')

        for row in excel_data:

            
                
            egresado = egresadoModel.objects.filter(identificacion = row[3])
            a = egresadoModel.objects.filter(correo = row[0])
            b = User.objects.filter(username = row[0])

            if not (egresado or a or b):

                PASSWORD = ''.join(random.choices(string.ascii_letters + string.digits, k=12))

                user       = User()
                egresado   = egresadoModel()
                estudiosUC = estudiosUCModel()
                
                user.username   = row[0]
                user.set_password(PASSWORD)
                user.first_name = row[1]
                user.last_name  = row[2]

                egresado.email          = row[0]
                egresado.identificacion = row[3]
                egresado.confianza      = True

                estudiosUC.titulo        = row[6]
                estudiosUC.nombre        = row[4]
                estudiosUC.fecha         = row[5].split(' ')[0]

                user.save()
                grupo.user_set.add(user)
                egresado.user            = user
                egresado.save()
                estudiosUC.egresadoModel = egresado
                estudiosUC.save()

                try:
                    url = settings.HOST + "/SIGEUC/formulario/{}".format(user.id)
                    msg.set_payload("Bienvenido, al SIGEUC. Le ha sido asignado un usuario en el mismo, para que, como egresado de la Universidad de Carabobo, pueda ingresar informacion. Su usuario es: {} , la clave es: {} . Si, lo desea, podes responder el siguiente formulario : {}".format(row[0], PASSWORD, url))
                    server.sendmail(settings.EMAIL_HOST_USER,row[0], msg.as_string())

                    correo = correosModel()
                    correo.usuario = row[0]
                    correo.status  = "Enviado"
                    correo.error   = "Ninguno"

                except:
                    
                    
                    correo = correosModel()
                    correo.usuario = row[0]
                    correo.status  = "No enviado"
                    correo.error   = "Sin acceso a internet"

                correo.save()

        
        return HttpResponse(status=200)

    else:

        return HttpResponse(status=400) 

def linkedin(request):

    from linkedin_v2 import linkedin
    import requests
    
    authentication = linkedin.LinkedInAuthentication(settings.API_KEY, settings.API_SECRET, settings.RETURN_URL, ['r_basicprofile', 'r_emailaddress', 'r_liteprofile'])
    authentication.authorization_code = request.GET['code']
    token = authentication.get_access_token().access_token

    headers = {'x-li-format': 'json', 'Content-Type': 'application/json'}
    params = {'oauth2_access_token': token}
    html = requests.get("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,public-profile-url,positions,specialties,summary)",headers=headers,params =   params)

    user = User.objects.get(id = int(request.GET['state']))
    egresado = egresadoModel.objects.get(user = user)

    if 'publicProfileUrl' in html.json().keys():
        egresado.url = html.json()['publicProfileUrl'].replace("http","https")

    if 'summary' in html.json().keys():
        egresado.descripcion = html.json()['summary']

    
    if 'specialties' in html.json().keys():
        egresado.intereses = html.json()['specialties']

    egresado.linkedin = True
    egresado.save()

    if 'positions' in html.json().keys():
        for position in html.json()['positions']['values']:
            trabajo = trabajosModel()
            trabajo.egresadoModel = egresado
            trabajo.titulo = position['company']['name']
            trabajo.nombre = position['title']
            trabajo.save()

    return redirect('/SIGEUC/inicio/egresado/privado/')

def login(request, itemEgresado):

    try:
        user = User.objects.get(id = itemEgresado)
        egresado = egresadoModel.objects.get(user = user)
        egresado.usada = True
        egresado.save()

    except:
        pass

    return HttpResponse(status = 200)

def consultaApi(tipo, estatus, registros):

    consulta = consultasModel()

    consulta.tipo   = tipo
    consulta.fecha  = datetime.now()
    consulta.status = estatus
    consulta.count  = registros

    consulta.save()

def api(request, confianza):

    consulta = consultasModel()
    
    try:
        
        egresados = egresadoModel.objects.filter(confianza__gte = float(confianza), confianza__lte = float(100))
        azucar    = [item.conexiones for item in egresados] + [1,2,3,4]
        intervalo = st.t.interval(0.95, len(azucar)-1, loc=np.mean(azucar), scale=st.sem(azucar))  
        pdb.set_trace()
        egresados = egresadoModel.objects.filter(conexiones__gte = intervalo[0], conexiones__lte = intervalo[1])

        listEgresados = []

        for item in egresados:
            dictItem = {}

            dictItem["primernombre"]   = item.user.first_name
            dictItem["primerapellido"] = item.user.last_name
            dictItem["email"]          = item.user.username
            dictItem["telefono"]       = item.telefono
            dictItem["identificacion"] = item.identificacion
            dictItem["codigo"]         = item.user.id

            listEgresados.append(dictItem)

        trabajos = trabajosModel.objects.all()
        listTrabajos = []

        for item in trabajos:
            dictItem = {}

            dictItem["egresado"]      = item.egresadoModel.user.id
            dictItem["codigo"]        = item.id
            dictItem["nombreempresa"] = item.titulo
            dictItem["cargo"]         = item.nombre
            dictItem["descripcion"]   = item.descripcion
            dictItem["informacion"]   = 0
            dictItem["laborando"]     = False

            listTrabajos.append(dictItem)

        UC = estudiosUCModel.objects.all()
        listEstudiosUC = []

        for item in UC:
            dictItem = {}

            dictItem["egresado"]         = item.egresadoModel.user.id
            dictItem["codigo"]           = item.id
            dictItem["facultad"]         = item.titulo
            dictItem["titulo"]           = item.descripcion

            listEstudiosUC.append(dictItem)

        consultaApi("Egresado", "Exitoso", egresados.count())

        result = {
                    "status":"Ok",
                    "dim-egresado": {
                        "items": listEgresados
                    },
                    "dim-estudiosuc": {
                        "items": listEstudiosUC
                    },
                    "dim_trabajo": {
                        "items": listTrabajos
                    }
                 }

        return JsonResponse(result, safe = False)

    except:
        
        result = {
                    "status":"Error",
                    "dim-egresado": {
                        "items": []
                    },
                    "dim-estudiosuc": {
                        "items": []
                    },
                    "dim_trabajo": {
                        "items": []
                    }
                 }

        consultaApi("Egresado", "Error", 0)
        return JsonResponse(result, safe = False)

def toXLS(name, columns, rows):

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(name)

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Api Usos')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

def export_apiUsos_xls(request):

    columns = ['ID', 'Tipo', 'Fecha', 'Estado', 'Numero de registros', ]
    name    = "apiUsos.xls"
    rows    = consultasModel.objects.all().values_list('id', 'tipo', 'fecha', 'status', 'count')

    return toXLS(name, columns, rows)

def export_appUsos_xls(request):

    columns = ['ID', 'Nombre', 'Apellido', 'Cedula', 'Correo', 'Uso aplicacion', 'Uso Linkedin', ]
    name    = "appUsos.xls"
    rows    = [[item.user.id,
              item.user.first_name,
              item.user.last_name,
              item.identificacion,
              item.correo,
              str(item.usada),
              str(item.linkedin)] for item in egresadoModel.objects.all()]

    return toXLS(name, columns, rows)

def export_emailSend_xls(request):

    columns = ['ID', 'Usuario', 'Status', 'Error']
    name    = "correos.xls"
    rows    = [[item.id, item.usuario, item.status, item.error] for item in correosModel.objects.all()]

    return toXLS(name, columns, rows)

def reenviarCorreos(request):
    
    server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)
    server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        
    import email.message
    msg = email.message.Message()
    msg['Subject'] = 'SIGEUC'
    msg.add_header('Content-Type', 'text/html')

    for item in correosModel.objects.filter(status = "No enviado"):

        PASSWORD = ''.join(random.choices(string.ascii_letters + string.digits, k=12))
        user = User.objects.get(username = item.usuario)
        user.set_password(PASSWORD)
        user.save()
        url = settings.HOST + "/SIGEUC/formulario/{}".format(user.id)
        msg.set_payload("Bienvenido, al SIGEUC. Le ha sido asignado un usuario en el mismo, para que, como egresado de la Universidad de Carabobo, pueda ingresar informacion. Su usuario es: {} , la clave es: {} . Si, lo desea, podes responder el siguiente formulario : {}".format(item.usuario, PASSWORD, url))
        server.sendmail(settings.EMAIL_HOST_USER,item.usuario, msg.as_string())
        item.status = "Enviado"
        item.save()

    return redirect('/SIGEUC/  inicio/admin/')
        
def estadisticosAdmin(request):

    result = {}
    api = consultasModel.objects.all()
    
    if api.count():
        countApi = api.count()
        result['apiOkApi'] = int((api.filter(status = "Exitoso").count() / countApi) * 100)

    correo = correosModel.objects.all()

    if api.count():
        countApi = api.count()
        result['email'] = int((correo.filter(status = "No enviado").count() / countApi) * 100)

    estadisticos = correoSistemaModel.objects.all()
    if estadisticos.exists():
        estadistica = estadisticos[0]

        result["emailTLS"]          = estadistica.emailTLS
        result["emailHost"]         = estadistica.emailHost
        result["emailPort"]         = estadistica.emailPort
        result["emailBackEnd"]      = estadistica.emailBackEnd
        result["emailHostUser"]     = estadistica.emailHostUser
        result["emailHostPassword"] = estadistica.emailHostPassword

    
    estadisticos = apiModel.objects.all()
    if estadisticos.exists():
        estadistica = estadisticos[0]

        result["IdCliente"]          = estadistica.claveCliente
        result["claveCliente"]       = estadistica.claveCliente
    
    return JsonResponse(result, safe = False)

def estadisticosVicerrector(request):
    
    result = {}
    
    egresado = egresadoModel.objects.all()
    
    result['usoApp'] = 0
    result['usoLinkedin'] = 0

    if egresado.count():

        countEgresado = egresado.count()
    
        result['usoApp'] = int((egresado.filter(usada = True).count() / countEgresado) * 100)
        result['usoLinkedin'] = int((egresado.filter(linkedin = True).count() / countEgresado) * 100)

    return JsonResponse(result, safe = False)

def form(request, id):
    
    try:
    
        user = User.objects.get(id = id)
        egresado = egresadoModel.objects.get(user = user)
        trabajo = trabajosModel()

        user.first_name = request.POST["nombre"]
        user.last_name  = request.POST["apellido"]
        egresado.identificacion = request.POST["cedula"]
        egresado.telefono  = request.POST["telefono"]
        trabajo.egresadoModel = egresado
        trabajo.titulo = request.POST["empresa"]
        trabajo.nombre = request.POST["cargo"]
        
        egresados = []
        egresados.append(request.POST["primerEgresado"])
        egresados.append(request.POST["segundoEgresado"])

        for egresado in egresados:
            try:
            
                user = User.objects.get(username = egresado)
                egresado = egresadoModel.objects.get(user = user)
                egresado.conecciones = egresado.conecciones + 1
                egresado.save()
            
            except:

                try:

                    PASSWORD = ''.join(random.choices(string.ascii_letters + string.digits, k=12))
                    user = User(username = egresado)
                    user.set_password(PASSWORD)
                    user.save()
                    egresadoModel(user = user).save()
                    url = settings.HOST + "/SIGEUC/formulario/{}".format(user.id)
                    msg.set_payload("Bienvenido, al SIGEUC. Le ha sido asignado un usuario en el mismo, para que, como egresado de la Universidad de Carabobo, pueda ingresar informacion. Su usuario es: {} , la clave es: {} . Si, lo desea, podes responder el siguiente formulario : {}".format(item.usuario, PASSWORD, url))
                    server.sendmail(settings.EMAIL_HOST_USER,item.usuario, msg.as_string())
                
                except:
                    pass
    except:
        pass

    return HttpResponse(status=200)

def actualizarEmail(request):

    correoSistemaModel.objects.all().delete()
    correo = correoSistemaModel()

    correo.emailTLS          = request.POST["emailTLS"]
    correo.emailHost         = request.POST["emailHost"]
    correo.emailPort         = request.POST["emailPort"]
    correo.emailBackEnd      = request.POST["emailBackEnd"]
    correo.emailHostUser     = request.POST["emailHostUser"]
    correo.emailHostPassword = request.POST["emailHostPassword"]

    correo.save()
    
    return HttpResponse(status=200)

def actualizarApi(request):
    
    apiModel.objects.all().delete()
    api = apiModel()
    pdb.set_trace()
    api.IDCliente          = request.POST["IdCliente"]
    api.claveCliente       = request.POST["claveCliente"]

    correo.save()
    return HttpResponse(status=200)
