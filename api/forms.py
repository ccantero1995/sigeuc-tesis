from django import forms
from django.forms import ModelForm
from api.models import *

class egresadoForms(ModelForm):
	class 	Meta:
		model   = egresadoModel
		fields  = [ 'intereses', 'descripcion', 
					'telefono','correo', 'url', 'direccion']

class trabajosForms(ModelForm):
	class 	Meta:
		model = trabajosModel
		fields = ['titulo', 'nombre', 'descripcion']
class estudiosUCForms(ModelForm):
	class 	Meta:
		model = estudiosUCModel
		fields = ['titulo', 'nombre', 'fecha', 'descripcion']