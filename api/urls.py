from django.urls import path
from api import views

urlpatterns = [
    path('linkedin/', views.linkedin),

    path('egresado/trabajo/<int:id>/<str:token>', views.trabajo),

    path('egresado/estudioUC/<int:id>/<str:token>', views.estudioUC),

    path('egresado/<int:itemEgresado>/completo', views.completo),

    path('egresado/<int:itemEgresado>/login', views.completo),

    path('egresado/datos/<int:confianza>', views.api),

    path('egresado/dataApi/', views.export_apiUsos_xls, name = "api_usos_xls"),
    path('egresado/dataApp/', views.export_appUsos_xls, name = "app_usos_xls"),
    path('egresado/dataCorreo/', views.export_emailSend_xls, name = "app_correo_xls"),

    path('egresado/estadisticos/admin/', views.estadisticosAdmin),
    path('egresado/estadisticos/vicerrector/', views.estadisticosVicerrector),

    path('cargador/carga/<str:token>', views.digaeCarga),
    path('form/carga/<int:id>', views.form),

    path('actualizar/email/', views.actualizarEmail),
    path('actualizar/api/', views.actualizarApi),
    path('reenviar/correo', views.reenviarCorreos, name = "reenviar_correo"),

]