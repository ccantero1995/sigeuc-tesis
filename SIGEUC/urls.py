from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from . import settings

urlpatterns = [
    path('SIGEUC/api/v1/', include('api.urls')),
    path('SIGEUC/admin/', admin.site.urls),
    path('SIGEUC/', include('interfaz.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)