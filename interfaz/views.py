from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from django.contrib.auth import logout, login
from django.contrib.auth.models import User, Group
from django.conf import settings
import requests, pdb, smtplib, random, string




def inicio(request):

	redirec =   {
					'Inicio'    : '#inicio',
					'Egresado'  : '#egresado', 
					'Cargador'  : '#digae',
					'Administrador': '#admin',
					'vicerrectorado': '#vice'
				}

	template = loader.get_template('SIGEUC/inicioSesion.html')
	return HttpResponse(template.render({ 'dictMenu':redirec, 'image':'UC' }, request))

def sesion(request, grupo):
	
	result  = {'status':'Error'}
	valido  = False
	
	if (request.method == 'POST') and ('usuario' in request.POST.keys()) and ('clave' in request.POST.keys()):
		usuario = authenticate(username = request.POST['usuario'], password = request.POST['clave'])

		if (usuario is not None) and (usuario.is_active) and  bool(usuario.groups.filter(name = grupo)):
				
			logout(request)
			login(request, usuario)
			valido = True

	elif request.method == 'GET' and request.user.is_authenticated and  bool(request.user.groups.filter(name = grupo)):

		usuario = User.objects.get(username = request.user.username)
		valido = True

	if valido:
		Token.objects.filter(user = usuario).delete()
		result['status']  = 'Ok'
		result['token']   = Token.objects.create(user=usuario).key
		result['user']    = usuario.id
		result['request'] = request

	return result

def cerrarSesion(request):

	
	if request.user.username:

		usuario = User.objects.get(username = request.user.username)
		Token.objects.filter( user = usuario).delete()
		logout(request)

	return redirect('/SIGEUC/inicio')

def cerrarCuenta(request):

	try:
		
		usuario = User.objects.get(username = request.user.username)
		token   = Token.objects.filter(user = usuario)
		
		token.delete()
		usuario.delete()

	except:

		pass

	return redirect('/SIGEUC/inicio')

def alInicio(request):
	
	return redirect('/SIGEUC/inicio')

def ini(request, id):

	return alInicio(request)

def linkedin(request):

	from linkedin_v2 import linkedin

	query    = sesion(request, 'egresado') 

	if query['status'] == 'Ok':
		
		authentication = linkedin.LinkedInAuthentication(settings.API_KEY, settings.API_SECRET, settings.RETURN_URL, ['r_basicprofile', 'r_emailaddress', 'r_liteprofile'])
		authentication.state = str(query['user'])
		return redirect(authentication.authorization_url)

	else:

		return redirect('/SIGEUC/inicio')

def formulario(request, id):

	result = {}

	result['redirec'] =   {
						'Inicio'    : '#inicio',
						'Formulario' : '#carga'
					   }
	result['user'] = str(id)

	template = loader.get_template('SIGEUC/form.html')
	return HttpResponse(template.render(result, request))

#Usuarios

def admin(request):
	
	query    = sesion(request, 'administrador')

	if query['status'] == 'Ok':
		
		url          = "http://" + settings.HOST + "/SIGEUC/api/v1/egresado/estadisticos/admin/"
		response     = requests.get(url)
		result       = response.json()

		result['dictMenu'] =   {
						'Inicio'              : '#inicio',
						'Estadisticos'        : '#estadistico',
						'Configuraciòn correo': '#correo',
						
					   }


		template = loader.get_template('SIGEUC/admin/principal.html')
		return HttpResponse(template.render(result, request))

	return redirect('/SIGEUC/inicio')

def vicerrectorado(request):
	
	query    = sesion(request, 'vicerrectorado')

	if query['status'] == 'Ok':
	
		url          = "http://" + settings.HOST + "/SIGEUC/api/v1/egresado/estadisticos/vicerrector/"
		response     = requests.get(url)
		result       = response.json()

		result['dictMenu'] =   {
						'Inicio'              : '#inicio',
						'Estadisticos'        : '#estadistico',
						'Usuario de datos'    : '#userDatos',
					   }


		template = loader.get_template('SIGEUC/vicerrectorado/principal.html')
		return HttpResponse(template.render(result, request))

	return redirect('/SIGEUC/inicio')

def egresado(request):
	
	query    = sesion(request, 'egresado') 

	if query['status'] == 'Ok':

		url          = "http://" + settings.HOST + "/SIGEUC/api/v1/egresado/{}/login".format(query['user'])
		requests.get(url)
		redirec =   {
							'Inicio'            : '#inicio',
							'Perfil'            : '#perfil',
							'Publicaciones'     : '#publicaciones',
							'Patentes'          : '#patentes',
							'Idiomas'           : '#idiomas',
							'Certificaciones'   : '#certificaciones',
							'Educacion'         : '#educacion',
							'Cursos'            : '#cursos',
							'Trabajos'          : '#trabajos',
							'Voluntariados'     : '#voluntariados',
							'Estudios en la UC' : '#estudiosUC',
					}
		url          = "http://" + settings.HOST + "/SIGEUC/api/v1/egresado/{}/completo".format(query['user'])
		response     = requests.get(url)
		
		if response.status_code == 200:
				
			response = response.json()
			template = loader.get_template('SIGEUC/egresado/egresadoPerfilPrivado.html')
			
			render   =  { 
						'dictMenu':redirec, 
						'perfil':response['perfil'],
						'publicaciones':response['publicaciones'],
						'patentes': response['patentes'],
						'certificaciones': response['certificaciones'],
						'cursos': response['cursos'],
						'idiomas': response['idiomas'],
						'educacion': response['educacion'],
						'trabajos': response['trabajos'],
						'voluntariados': response['voluntariados'],
						'estudiosUC': response['estudiosUC'],
						'api':{'token':query['token'], 'user':query['user']}, 
						'perfilUrl': "http://" + settings.HOST + "/SIGEUC/inicio/egresado/publico/" + str(query["user"]) + "/"
						}

			if response['perfil']['foto'] != '':
				render['image'] = "si"
			else:
				render['image'] = "no"
			
			result = HttpResponse(template.render(render, request))

		else:

			return redirect('/SIGEUC/inicio#egresado')

	else:
			
		return redirect('/SIGEUC/inicio#egresado')

	return result

def cargador(request):

	query    = sesion(request, 'cargador')

	if query['status'] == 'Ok':

		redirec =   {
						'Inicio'       : '#inicio'
					}
	
		template = loader.get_template('SIGEUC/DIGAE/carga.html')
		response =  HttpResponse(template.render({ 'dictMenu':redirec, 'image':'DIGAE', 'api':{'token':query['token'], 'user':query['user']} }, request))

		return response

	else:

		return redirect('/SIGEUC/inicio#digae')

def registrar(request):
    
    try:
        User.objects.get(username = request.POST["name"])
    except:
        
        try:
            user = User()
            grupo = Group.objects.get(name='cargador')
            user.username   = request.POST["name"]
            user.first_name = request.POST["confianza"]
            PASSWORD = ''.join(random.choices(string.ascii_letters + string.digits, k=12))
            user.set_password(PASSWORD)
            user.save()
            grupo.user_set.add(user)

            server = smtplib.SMTP_SSL(settings.EMAIL_HOST, settings.EMAIL_PORT)
            server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        
            import email.message
            msg = email.message.Message()
            msg['Subject'] = 'SIGEUC'
            msg.add_header('Content-Type', 'text/html')
        
            msg.set_payload("Bienvenido, al SIGEUC. Le ha sido asignado un usuario en el mismo, para que, ingrese data de egresados de la Universidad de Carabobo. Su usuario es: {} , la clave es: {} . ".format(request.POST["name"], PASSWORD))
            server.sendmail(settings.EMAIL_HOST_USER,request.POST["name"], msg.as_string())
        except:
        	pass
    return redirect('/SIGEUC/inicio/vicerrectorado/')

def eliminar(request):
    try:
        User.objects.get(username = request.POST["name"]).delete()
    except:
    	pass

    return redirect('/SIGEUC/inicio/vicerrectorado/')